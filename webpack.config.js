const path = require('path');
const os = require("os");
const ifaces = os.networkInterfaces();

module.exports = {
    mode: 'development',
    entry: path.join(__dirname, 'src', 'index'),
    output: {
        filename: 'bundle.js',
        path: path.resolve(__dirname, 'build')
    },
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                use: 'ts-loader',
                exclude: /node_modules/
            }
            // {
            //     test: /\.html?$/,
            //     use: 'html-loader',
            //     exclude: /node_modules/
            // }
        ]
    },
    resolve: {
        extensions: ['.json', '.js', '.jsx', '.css', '.ts', '.html']
    },
    devtool: 'source-map',
    devServer: {
        contentBase: "./build",
        host: getLocalIP(),
        port: 8081,
        hot: false
    }
};

function getLocalIP() {
    let neededAddr = undefined;

    Object.keys(ifaces).forEach(function (ifname) {
        var alias = 0;
        ifaces[ifname].forEach(function (iface) {
            if ('IPv4' !== iface.family || iface.internal !== false) {
                // skip over internal (i.e. 127.0.0.1) and non-ipv4 addresses
                return;
            }
            if (neededAddr === undefined) {
                neededAddr = iface.address;
                return;
            }
            ++alias;
        });
    });

    return neededAddr;
}